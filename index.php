<?php
include 'product.php';

$productObj = new Product();

if(isset($_POST['delete'])){
	$check=$_POST['chk_id'] ?? null;
	if(is_array($check)){
	    foreach($check as $arr){
		    $productObj->deleteProduct($arr);
    	}
	}
}
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Product List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>	
</head>
<body>
    <div class="card text-center">
    <h2>Product List</h2>
</div><br><br> 
<div class="container">
 
  <table class="table table-hover">
    <thead>
	<form method="post" action="">
   <input class="btn btn-primary" type="submit" name="delete" style="float:right" id="delete" value="Mass Delete">
   
   <p><a href="add.php" class="btn btn-primary" style="float:center;">Add New Product</a></p>
   
      <tr>
        <th>SKU</th>
        <th>Name</th>
        <th>Price</th>
        <th>Attribute</th>
		<th><input id="select_all" name="select_all" class="checkbox" type="checkbox"> </th>
      </tr>
    </thead>
	<tbody>
        <?php 
          $products = $productObj->showProduct(); 
		  if (is_array($products) || is_object($products)){
          foreach ($products as $product){
        ?>
        <tr>
		<td><?php echo $product['SKU'] ?> </td>
          <td><?php echo $product['name'] ?></td>
          <td><?php echo number_format($product['price'], 2) ?> $</td>
          <td><?php echo $product['attribute']?></td>
		  <td><input type="checkbox" id="checkbox" class="checkbox" name="chk_id[]" value="<?php echo $product['SKU'] ?>" ></input></td>
		  </tr>
		  <?php } }?>
    </tbody>
	</form>
  </table>
</div>

<script type="text/javascript">
$("#select_all").change(function(){  
	var status = this.checked; 
	$('.checkbox').each(function(){ 
		this.checked = status; 
	});
});


</script>
</body>
</html>