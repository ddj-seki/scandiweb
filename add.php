<?php
	include 'product.php';
	
	$productObj = new Product();
	
	if(isset($_POST['submit'])) {
		$productObj->addProduct($_POST);
	}
	

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Product Add</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
</head>
<style>
.hidden {
    display: none;
}
</style>
<body>

<div class="card text-center">
  <h2>Product Add</h2>
</div><br> 

<div class="container">
  <form action="add.php" method="POST" id="form">
	<div class="form-group">
	  <label for="SKU">SKU:</label>
	  <input type="text" class="form-control" name="SKU" placeholder="Enter SKU" required="">
    </div>
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" name="name" placeholder="Enter name" required="">
    </div>
    <div class="form-group">
      <label for="price">Price:</label>
      <input type="float" class="form-control" name="price" placeholder="Enter price" required="">
    </div>
    <div class="form-group">
      <label for="attribute">Attribute:</label>
	  <select name="" id="id" class="form-control" required="" >
	  <option value="">- Select -</option>
	  <option value="Size(MB)">Size</option>
	  <option value="Weight(KG)">Weight</option>
	  <option value="Dimensions">Dimensions</option>
	  </select></div>
	  <div class="form-group" id="nestoo">
	  
	  </div>
	
	<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
	
	<script>
	
	$("#id").change(function(){          
    var value = $("#id option:selected").val();
	var index = $("#id").prop('selectedIndex');
	if(index < 3) {
	$("#nestoo").html("<input type='number' id='broj' name='attributee' class='form-control' placeholder='" + value + "' required=''>"); 
	}
	else {
	$("#nestoo").html("<input type='number' class='form-control' id='one' placeholder='Height' required=''>" +
	"<input type='number' id='two' class='form-control' placeholder='Width' required=''>" +
	"<input type='number' id='three' class='form-control' placeholder='Length' required='' >");
	 
	}
	});
   

$(document).ready(function(){
	$("#btn").click(function(){
	var proba = $('#id option:selected').val();
	var index = $("#id").prop('selectedIndex');
	var broj = $("#broj").val();
	$("#text").val(proba + ": " + broj);
	if(index == 3){
		$("#text").val(proba + ": " + $("#one").val() + "x" + $("#two").val() + "x" + $("#three").val())
	}
	});
});
	</script>
	<input type="text" id="text" name="attribute" class="hidden">
	<input type="submit" name="submit" class="btn btn-primary" id="btn" style="float:right;" value="Save">
	<a href="index.php" class="btn btn-primary" >Cancel</a>
  </form>
</div>

</body>
</html>